package main

import (
	"fmt"
	"sync"
	"strconv"
	"spider_test/service"
	"spider_test/database"
)

var pageCount = 10
var current = 1
var xici_count = 0
var wg sync.WaitGroup

func getBooks(url string, ips chan string, ch chan database.Book, wg *sync.WaitGroup) {
	ip := <-ips
	res, err := service.ProxyRequest(ip, url)
	for err != nil {
		fmt.Printf("http failed:%s\n",err)
		if (len(ips) == 0 && xici_count == current) {
			wg.Add(1)
			go service.GetProxyIp(ips, wg)
			xici_count += 1
		}
		ip = <-ips
		res, err = service.ProxyRequest(ip, url)
	}
	ips <- ip
	pageCount = pageCount - 1
	fmt.Printf("http success\n")
	service.QueryBooks(res, ch)
	if (xici_count > current) {
		current += 1
	}
	defer wg.Done()
}

func main() {
	key := 0

	url := "https://book.douban.com/top250?start="
	
	dataCh := make(chan database.Book, 100)
	ipCh := make(chan string, 100)
	
	wg.Add(1)
	go service.GetProxyIp(ipCh, &wg)
	xici_count += 1

	wg.Add(1)
	go service.Save(dataCh, &wg)

	for {
		if (key < 10) {
			wg.Add(1)
			go getBooks(url + strconv.Itoa(key * 25), ipCh, dataCh, &wg)
			key += 1 
		} else {
			break
		}
	}

	for pageCount < 1 {
		close(dataCh)
		close(ipCh)
	}
	wg.Wait()
}