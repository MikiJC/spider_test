package service

import (
	"net/http"
	"net/url"
	"time"
	"log"
	"fmt"
)

func getTransport(ip string) *http.Transport {
	proxy, err := url.Parse(ip)

	if err != nil {
		panic(err)
	}

	return &http.Transport{
		Proxy : http.ProxyURL(proxy),
	}
}

func ProxyRequest(ip string, u string) (*http.Response, error) {

	fmt.Printf("url: %s\nproxyIp: %s\n", u, ip)

	req, _ := http.NewRequest("GET", u, nil)
	req.Header.Set("connection", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36")

  client := &http.Client{
		Transport: getTransport(ip),
		Timeout: time.Duration(50 * time.Second),
	}
        
	res, err := client.Do(req)

	return res, err
}

func Request(u string) *http.Response {

	req, _ := http.NewRequest("GET", u, nil)
	// req.Header.Set("connection", "keep-alive")
	// req.Header.Set("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
	req.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36")

  client := &http.Client{}
        
  return getBody(req, client)
}

func getBody(req *http.Request, client *http.Client) *http.Response {
	res, err := client.Do(req)
    
	if err != nil {
			panic(err)
	}

	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	return res
}