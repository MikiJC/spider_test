package service

import (
	"fmt"
	"net/http"
	"spider_test/database"
	"github.com/PuerkitoBio/goquery"
	"sync"
)

func QueryBooks(res *http.Response, ch chan database.Book) {
	doc, err := goquery.NewDocumentFromReader(res.Body)

	if err != nil {
		panic(err)
	}

	doc.Find(".indent table").Each(func(i int, s *goquery.Selection) {
		name := FormatTitle(s.Find(".item .pl2 a").Text())
		detail := FormatDetail(s.Find(".item .pl").First().Text())
		score := s.Find(".item .star .rating_nums").Text()
		fmt.Printf("%s", detail)
		ch <- FormatBook(name, detail, score)
	})

	defer res.Body.Close()
}

func Save(ch chan database.Book, wg *sync.WaitGroup) {
	for {
    if newBook, ok := <-ch; ok {
			database.InsertBook(newBook)
    } else {
      break
    }
	}
	defer wg.Done()
}
