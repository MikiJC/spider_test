package service

import ( 
	"regexp"
	"strings" 
	"spider_test/database"
	"strconv"
)

func FormatTitle(str string) string {
	result := strings.Replace(str, "\n", "", -1)
	result = strings.Replace(result, " ", "", -1)
	return result
}

func FormatDetail(str string) []string {
	return strings.Split(str, "/")
}

func FormatFloat32(str string) (float32) {
	data, err := strconv.ParseFloat(
		strings.TrimSpace(str), 32)
	if err != nil {
		panic(err)
	}
	result := float32(data)
	return result
}

func FormatBook(name string, detail []string, score string) database.Book {
	var book database.Book
	for i := 0; i < len(detail); i += 1 {
		str := strings.TrimSpace(detail[i])
		switch true {
			case strings.Contains(str, "-"):
				book.Date = str
			case strings.Contains(str, "出版"):
				book.PStore = str
			case ValidPrice(str):
				book.Price = FormatFloat32(ToNum(str))
			default:
				if i == 0 { book.Author = str }
		}
	}
	book.Name = strings.TrimSpace(name)
	book.Score = FormatFloat32(strings.TrimSpace(score))
	return book
}

func ValidPrice(str string) bool {
	reg := regexp.MustCompile("^[0-9]+.[0-9]{0,2}")
	return reg.MatchString(str)
}

func ToNum(str string) string {
	reg := regexp.MustCompile("[^\\d.]")
	return reg.ReplaceAllString(str, "")
}