package service

import (
	"fmt"
	"sync"
	"strconv"
	"strings"
	"net/http"
	"github.com/PuerkitoBio/goquery"
)

var xici_url = "https://www.xicidaili.com/nn/"
var key = 1

func queryIps(res *http.Response, ch chan string) {
	doc, err := goquery.NewDocumentFromReader(res.Body)
	
	if err != nil {
		panic(err)
	}

	doc.Find("#ip_list .odd").Each(func(i int, s *goquery.Selection) {
		ip := s.Find("td").Eq(1).Text()
		port := s.Find("td").Eq(2).Text()
		protocol := s.Find("td").Eq(5).Text()
		str := strings.ToLower(protocol) + "://" + ip + ":" + port
		ch <- str
	})

	defer res.Body.Close()

}

func GetProxyIp(ch chan string, wg *sync.WaitGroup) {
	fmt.Printf("* xici_urt:%s\n", xici_url + strconv.Itoa(key))
	res := Request(xici_url + strconv.Itoa(key))
	key += 1
	queryIps(res, ch)
	defer wg.Done()
}
