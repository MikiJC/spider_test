package database

import  (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
)

var db *mgo.Database

type Book struct {
	Id bson.ObjectId `bson:"_id"`
	Name string `bson:"name"`
	Author string `bson:"author"`
	PStore string `bson:"pstore"`
	Date string `bson:"date"`
	Price float32 `bson:"price"`
	Score float32 `bson:"score"`
}

func connect() *mgo.Database {
	if db != nil { return db  }
		
	var url = "118.31.10.250:27017"

	session, err := mgo.Dial(url)

	if err != nil {
		log.Fatal(err)
	}
	
	db = session.DB("douban")

	return db
}

func FindBooks() (Book, error){
	var book Book
	c := connect().C("books")
	err := c.Find(bson.M{}).One(&book)
	if err != nil {
		log.Fatal(err)
	}
	return book, err
}

func InsertBook(newBook Book) {
	c := connect().C("books")
	newBook.Id = bson.NewObjectId()
	err := c.Insert(&newBook)
	if err != nil { 
		panic(err) 
	}
}
